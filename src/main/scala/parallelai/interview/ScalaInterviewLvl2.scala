package parallelai.interview

/**
  *  Scala Test Level 2 (Intermediate to Advanced)
  *
  *  Create a called Holder that makes the following code work.
  *  val one = One("Roger", "Rabbit". 33)
  *
  *  // Create a Holder object from a Case Class
  *  val h: Holder = Holder(one)
  *
  *  // Get the value of a labelled field
  *  val age: Int = h('age)
  *  val fn: String = h('fname)
  *  val ln: String = h('lname)
  *
  *  val lm: (Boolean, Boolean) = if (age < 18) (true, false) else (false, true)
  *
  *  // Append Fields
  *  val h2 = h append ('less18, lm._1) append ('more18, lm._2)
  *
  *  // Swap first and last name
  *  val h3 = h3 update ('fname, ln) update ('lname, fn)
  *
  *  // Append another field
  *  val h4 = h3 append ('name, h3('fname) + "," + h3('lname))
  *
  *  // Map Holder to another Case Class
  *  val two = h.to[Two]
  *
  *  // Expect output
  *  val test_two = Two("Rabbit,Roger", false, true)
  *
  *  // Follwing equality should be true
  *  two == test_two
  *
  * NOTE: Create the appropriate Test Spec in the relevant location
  *
  */

case class One(fname: String, lname: String, age: Int)

case class Two(name: String, less18: Boolean, more18: Boolean)

