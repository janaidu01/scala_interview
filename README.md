# Scala Technical Interview

- Clone the repo
- Write the code
- Write the tests
- Add the test results to /reports folder
- Create a zip file of the whole project (after sbt clean)
- Email zip file to scala@parallelai.com

# Test Levels
- Level 1: Basic to Intermediate Scala
- Level 2: Intermediate to Advanced Scala
- Level 3: Advanced Scala

_Direct all questions to scala@parallelai.com_